import { Injectable } from '@angular/core';

declare const $: any;

@Injectable({
  providedIn: 'root',
})
export class ProjectDataService {
  constructor() {}

  private data: any;

  initializeState() {
    this.data = {
      LOADER_STACK: 0,
    };
    this.initializaStackableModals();
  }

  initializaStackableModals() {
    $('body').on('hidden.bs.modal', function () {
      if ($('.modal.fade.show').length > 0) {
        $('body').addClass('modal-open');
      }
    });
  }

  get(key) {
    return this.data[key];
  }

  set(key, data) {
    this.data[key] = data;
  }

  displayLoader() {
    $('#loader').show();
    this.data.LOADER_STACK++;
  }

  removeLoader() {
    if (this.data.LOADER_STACK > 0) {
      this.data.LOADER_STACK--;
    }

    if (this.data.LOADER_STACK == 0) {
      $('#loader').hide();
    }
  }

  triggerError(errorText) {
    $('#errorModalText').text(errorText);
    $('#errorModal').modal('show');
  }

  triggerSuccess(successText) {
    $('#successModalText').text(successText);
    $('#successModal').modal('show');
  }

  formatAmount = (value, roundUpFlag?) => {
    return !isNaN(this.cleanAmount(value))
      ? '$' +
          (roundUpFlag
            ? this.cleanAmount(value).toFixed(2)
            : this.cleanAmount(value).toString()
          ).replace(/(\d)(?=(\d{3})+(?!\d))/g, '$1,')
      : '';
  };

  cleanAmount = (value) => {
    return parseFloat(String(value).replace(/[$,]/g, ''));
  };
}
