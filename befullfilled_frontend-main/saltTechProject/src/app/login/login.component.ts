import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CONSTANTS } from '../constants/global-constants';
import { ProjectApiService } from '../project-api.service';
import { ProjectDataService } from '../project-data.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss'],
})
export class LoginComponent implements OnInit {
  isLoggedIn = false;
  isLoginFailed = false;
  errorMessage = '';
  roles: string[] = [];
  email: string = '';
  password: string = '';
  loginForm: NgForm;
  constructor(
    private router: Router,
    private PAS: ProjectApiService,
    private PDS: ProjectDataService
  ) {}

  ngOnInit(): void {}

  onLogin() {
    this.PDS.set('authToken', this.email);
    this.router.navigateByUrl('dashboard');
  }
}
