import { NgModule } from '@angular/core';
import { Route, RouterModule, Routes } from '@angular/router';
import { AuthGuard } from './auth.guard';
import { ChangepasswordComponent } from './changepassword/changepassword.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { ForgotpasswordComponent } from './forgotpassword/forgotpassword.component';
import { HelpComponent } from './help/help.component';
import { HomeComponent } from './home/home.component';
import { InvoicesComponent } from './invoices/invoices.component';
import { LoginComponent } from './login/login.component';
import { MessageComponent } from './message/message.component';
import { OrdersComponent } from './orders/orders.component';
import { ProjectsDetailsComponent } from './projects-details/projects-details.component';
import { ProjectsComponent } from './projects/projects.component';
import { RegisterComponent } from './register/register.component';
import { SettingsComponent } from './settings/settings.component';
import { UsersComponent } from './users/users.component';
import { ProjectsListComponent } from './projects-list/projects-list.component';

const dashboardRoutes: Route = {
  path: 'dashboard',
  component: DashboardComponent,
  canActivate: [AuthGuard],
  children: [
    {
      path: 'home',
      component: HomeComponent,
    },
    {
      path: 'orders',
      component: OrdersComponent,
    },
    {
      path: 'projects',
      component: ProjectsComponent,
    },

    {
      path: 'settings',
      component: SettingsComponent,
    },
    {
      path: 'users',
      component: UsersComponent,
    },
    {
      path: 'invoices',
      component: InvoicesComponent,
    },

    {
      path: 'help',
      component: HelpComponent,
    },
    {
      path: 'message',
      component: MessageComponent,
    },
    {
      path: 'projectDetails',
      component:ProjectsDetailsComponent ,
    },
    {
      path: 'projectslist',
      component:ProjectsListComponent,
    },
    { path: '', redirectTo: 'home', pathMatch: 'full' },
  ],
};

const routes: Routes = [
  {
    path: '',
    redirectTo: '/login',
    pathMatch: 'full',
  },
  {
    path: 'login',
    component: LoginComponent,
  },
  {
    path: 'register',
    component: RegisterComponent,
  },  
  {
    path: 'forgotPassword',
    component: ForgotpasswordComponent,
  },
  {
    path: 'changePassword',
    component: ChangepasswordComponent,
  },
  

  dashboardRoutes,
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
