import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { CONSTANTS } from './constants/global-constants';
import { ProjectDataService } from './project-data.service';

@Injectable({
  providedIn: 'root',
})

export class ProjectApiService {
 
  constructor(private http: HttpClient, private PDS: ProjectDataService) {}

  private BASE_URL = CONSTANTS.BASE_URL;
  private USERS = this.BASE_URL + '/api/users';
  private LOGIN = this.BASE_URL + '/users/login';
  private REGISTER = this.BASE_URL + '/users/register';

  getHeader(): any {
    return {
      headers: new HttpHeaders().set(
        'Authorization',
        'Bearer ' + this.PDS.get('authToken')
      ),
    };
  }
  login(credentials: any) {
    return this.http.post(this.LOGIN, credentials);
  }
  users(rqData: any) {
    return this.http.post(this.USERS, rqData, this.getHeader());
  }


  register(credentials) {
    return this.http.post(this.REGISTER,credentials);
  }
 

  // vendorPersonalInfo(rqData: any) {
  //   return this.http.post(this.VENDOR_PERSONAL_INFO, rqData, this.getHeader()).toPromise();
  // }
}
