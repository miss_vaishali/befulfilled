import { CONFIGS } from '../../../CONFIGS';

const SELECTED_CONFIG = CONFIGS.DEV;

export class CONSTANTS {
  static PATTERNS = {
    email: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,4}$/i,
    alphabets: /[A-z]/,
    alphaNumeric: /[A-z0-9]/,
    number: /^[0-9]*$/,
    cardNumber: /([0-9]{4}-){3}[0-9]{3,4}/,
    cardExpiry: /^(0[1-9]|1[0-2])\/[2-9][0-9]$/,
    cardCvv: /^[0-9]{3,4}$/,
    amountWithoutDecimals: /^[0-9$]*$/,
    vehicleNumber: /^[A-Z0-9]{1,8}$/,
  };

  static BASE_URL = SELECTED_CONFIG.BASE_URL;

  public static TECHNICAL_ERROR = 'Error Occurred. Please Contact Helpdesk.';
  public static API_SUCCESS = 'success';
  public static API_ERROR = 'error';
}
