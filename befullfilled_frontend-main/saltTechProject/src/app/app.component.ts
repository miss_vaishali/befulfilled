import { Component } from '@angular/core';
import { ProjectDataService } from './project-data.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss'],
})
export class AppComponent {
  constructor(private PDS: ProjectDataService) {}

  ngOnInit() {
    this.PDS.initializeState();
  }
}
