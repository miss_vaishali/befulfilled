import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Component, OnInit } from '@angular/core';
import { NgForm } from '@angular/forms';
import { Router } from '@angular/router';
import { CONSTANTS } from '../constants/global-constants';
import { ProjectApiService } from '../project-api.service';
import { ProjectDataService } from '../project-data.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss'],
})
export class RegisterComponent implements OnInit {
  patterns = CONSTANTS.PATTERNS;
  name = '';
  password = '';
  email = '';
  user = '';
  regForm: NgForm;
  // buttonDisabled: boolean | undefined;

  constructor(
    private router: Router,
    private PAS: ProjectApiService,
    private PDS: ProjectDataService,
    private http: HttpClient
  ) {}

  ngOnInit(): void {}
  onRegister(value): void {
    console.log(this.regForm.value);
    if (!this.regForm.valid) {
      return;
    }
    let rqData = {
      name: this.regForm.value.name,
      email: this.regForm.value.email,
      password: this.regForm.value.password,
    };
    this.http
      .post('http://localhost:3000/api/users/register', rqData)
      .subscribe(
        (resData) => {
          console.log(resData);
          this.router.navigate(['/login']);
        },
        (error) => {
          console.log(error);
        }
      );
    this.regForm.reset();
  }
}
